"""
This is what creates the expression sheets/body images

Todo:
    Find a way to combine body and faces, faces as normal but add bodies to the side
        I think to do this, i should create a completely different method from the first college, to make it easier
        

 

"""
### System ##
import os
import re
import sys
import math
import json
import copy
import argparse
import math
from glob import glob
from shutil import rmtree
from os.path import exists
from signal import signal, SIGINT, SIG_IGN
from multiprocessing import cpu_count, Pool

### Display ###
try:
    from tqdm import tqdm
except:
    print("Please run 'pip install tqdm'")
    sys.exit(1)

### YaML Parsing ###
try:
    import yaml
except:
    print("Please run 'pip install pyyaml'")
    sys.exit(1)

### Data Handling ###
try:
    import numpy as np
except:
    print("Please run 'pip install numpy'")
    sys.exit(1)

try:
    from PIL import Image, ImageDraw, ImageFont
except:
    print("Please run 'pip install pillow'")
    sys.exit(1)


def check(args):
    if not exists(args.input_dir):
        print("The input directory does not exist!")
        sys.exit(1)
        
    if not exists(args.face_dir) and not args.render_faces:
        print("The face directory does not exsist!")
        sys.exit(1)
        
    if exists(args.face_dir) and args.render_faces:
        if not args.yes:
            print("The face directory exists. Do you want to overwrite it?")
            result = input("[y]es/[n]o: ").lower()
            if result not in ["y", "yes"]:
                print("Aborted")
                sys.exit(0)
        rmtree(args.face_dir)
        os.makedirs(args.face_dir, exist_ok=True, mode=777)
        
    if not exists(args.bodies_dir) and not args.render_body:
        print("The bodies directory does not exsist!")
        sys.exit(1)
        
    if exists(args.bodies_dir) and args.render_body:
        if not args.yes:
            print("The bodies directory exists. Do you want to overwrite it?")
            result = input("[y]es/[n]o: ").lower()
            if result not in ["y", "yes"]:
                print("Aborted")
                sys.exit(0)
        rmtree(args.bodies_dir)
        os.makedirs(args.bodies_dir, exist_ok=True, mode=777)
        
    if exists(args.output_dir) and not args.sheets:
        if not args.yes:
            print("The output directory exists. Do you want to overwrite it?")
            result = input("[y]es/[n]o: ").lower()
            if result not in ["y", "yes"]:
                print("Aborted")
                sys.exit(0)
        rmtree(args.output_dir)
    if not args.sheets:
        os.makedirs(args.output_dir, exist_ok=True)


def natural_sort_key(s, _nsre=re.compile("([0-9]+)")):
    return [int(text) if text.isdigit() else text.lower() for text in _nsre.split(s)]


def slice_indexer_raw(row):
    left_side = 0
    for i, item in enumerate(row):
        if item > 0:
            left_side = i
            break

    right_side = 0
    for i, item in enumerate(row[::-1]):
        if item > 0:
            right_side = len(row) - i
            break

    return left_side, right_side, abs(right_side - left_side)

def render_collage_with_body(data):
    character_name, pose_name, faces, bodies, args = data
    
    total_images = len(faces)
    columns = int(math.sqrt(total_images))
    rows = int(math.ceil(total_images / columns))

    total_bodies = len(bodies)
    bcolumns = int(math.sqrt(total_bodies))
    brows = int(math.ceil(total_bodies / bcolumns))

    font_size = 24
    font = None
    try:
        font = ImageFont.truetype("arial_bold.ttf", font_size)
    except OSError as e:
        font = ImageFont.truetype("arial.ttf", font_size)
    all_imgs = [Image.open(face) for face in faces]
    all_bods = [Image.open(body) for body in bodies]
    img_width = max(img.width for img in all_imgs)
    img_height = max(img.height for img in all_imgs)
    bimg_width = max(img.width for img in all_bods)
    bimg_height = max(img.height for img in all_bods)

    rheight = (img_height + int(font_size * 1.1))
    brheight = (bimg_height + int(font_size * 1.1))

    if args.height and (brheight*brows) > (rows * rheight): 
        holding_power = bcolumns * brows
        brows = math.floor((rows * rheight) / brheight)
        brows = max(brows, 1)
        bcolumns = math.ceil(holding_power / brows)
        if brows == 1:
            bcolumns = total_bodies
            
    if args.height2 and (brheight*brows) > (rows * (img_height + int(font_size * 1.1))): 
        ind = 0
        while (rows * (img_height + int(font_size * 1.1))) > (brheight  * ind):
            ind += 1
        
        holding_power = bcolumns * brows
        brows = ind
        bcolumns = math.ceil(holding_power / brows)
        if brows == 1:
            bcolumns = total_bodies

    width = columns * img_width
    bwidth = bcolumns * bimg_width

    height = rows * (img_height + int(font_size * 1.1))
    bheight = brows * (bimg_height + int(font_size * 1.1))

    thumbnail_width = width // columns
    thumbnail_height = height // rows
    bthumbnail_width = bwidth // bcolumns
    bthumbnail_height = bheight // brows
    collage = Image.new("RGBA", (width+bwidth, max(height,bheight)))

    images = []
    for file in faces:
        name = os.path.splitext(os.path.basename(file))[0]
        image = Image.open(file).convert("RGBA")

        text = Image.new("RGBA", (img_width, font_size))
        draw = ImageDraw.Draw(text)
        draw.rectangle((0, 0, img_width, img_height), fill=(255, 255, 255, 255))
        text_width, text_height = font.getsize(name)
        draw.text(
            ((img_width // 2) - (text_width // 2), -int(font_size * 0.2)),
            name,
            fill=(0, 0, 0, 255),
            font=font,
        )

        annotated = Image.new("RGBA", (img_width, img_height + font_size))
        annotated.paste(image, (0, img_height - image.height))
        annotated.paste(text, (0, img_height))

        images.append(annotated)
    bimages = []
    for file in bodies:
        name = os.path.splitext(os.path.basename(file))[0]
        image = Image.open(file).convert("RGBA")

        text = Image.new("RGBA", (bimg_width, font_size))
        draw = ImageDraw.Draw(text)
        draw.rectangle((0, 0, bimg_width, bimg_height),
                       fill=(255, 255, 255, 255))
        text_width, text_height = font.getsize(name)
        draw.text(
            ((bimg_width // 2) - (text_width // 2), -int(font_size * 0.2)),
            name,
            fill=(0, 0, 0, 255),
            font=font,
        )

        annotated = Image.new("RGBA", (bimg_width, bimg_height + font_size))
        annotated.paste(image, (0, bimg_height - image.height))
        annotated.paste(text, (0, bimg_height))

        bimages.append(annotated)

    i, x, y, max_x = 0, 0, 0, 0
    for _ in range(rows):
        for _ in range(columns):
            if i >= len(images):
                break
            collage.paste(images[i], (x, y))
            i += 1
            x += thumbnail_width
            max_x = max(x, max_x)
        y += thumbnail_height
        x = 0

    i, x, y = 0, max_x, 0
    for _ in range(brows):
        for _ in range(bcolumns):
            if i >= len(bimages):
                break
            collage.paste(bimages[i], (x, y))
            i += 1
            x += bthumbnail_width
        y += bthumbnail_height
        x = max_x

    out_name = os.path.join(
        args.output_dir, "{}-{}.png".format(character_name, pose_name)
    )
    collage.save(out_name, optimize=True)


def extract_faces(data):
    character_name, pose_name, faces, outfit, accessorized_outfit, args = data
    opened_outfit = Image.open(outfit).convert("RGBA")
    if accessorized_outfit:
        accessories = [
            item
            for item in glob(os.path.join(os.path.dirname(outfit), "*"))
            if os.path.isdir(item)
        ]
        for accessory in accessories:
            accessory_filename_on = os.path.join(accessory, "on.png")
            accessory_filename_on_webp = os.path.join(accessory, "on.webp")
            accessory_filename_off = os.path.join(accessory, "off.png")
            accessory_filename_off_webp = os.path.join(accessory, "off.webp")
            on_exists = os.path.isfile(accessory_filename_on)
            on_exists_webp = os.path.isfile(accessory_filename_on_webp)
            off_exists = os.path.isfile(accessory_filename_off)
            off_exists_webp = os.path.isfile(accessory_filename_off_webp)
            accessory_filename = None
            if off_exists:
                accessory_filename = accessory_filename_off
            elif off_exists_webp:
                accessory_filename = accessory_filename_off_webp
            if args.accessory_switch:
                if on_exists:
                    accessory_filename = accessory_filename_on
                elif on_exists_webp:
                    accessory_filename = accessory_filename_on_webp
            if accessory_filename:
                accessory_image = Image.open(accessory_filename).convert("RGBA")
                opened_outfit.paste(accessory_image, mask=accessory_image)

    
    for face in faces[:1] if args.render_body else faces:
        name = os.path.splitext(os.path.basename(face))[0]
        expr_name = "{}_{}".format(pose_name, name)
        outfit_copy = opened_outfit.copy()
        face = Image.open(face).convert("RGBA")

        # Force all semi-transparent pixels to zero
        if not args.render_body:
            face_np = np.array(face)
            face_np[face_np < (0, 0, 0, 255)] = 0
            face = Image.fromarray(face_np)

        outfit_copy.paste(face, mask=face)

        if not args.render_body:
            face_width, face_height = face.size
            outfit_copy = outfit_copy.crop(
                (0, 0, outfit_copy.width, int(face_height * 1.07))
            )

            arr = np.array(outfit_copy)
            alpha_channel = arr[:, :, 3]
            result = np.apply_along_axis(
                slice_indexer_raw, axis=1, arr=alpha_channel.astype(np.int32)
            )

            top_y = 0
            for y, item in enumerate(result):
                val = sum(item)
                if val > 0:
                    top_y = y
                    break
            if alpha_channel.shape[0] <= 1 or alpha_channel.shape[1] <= 1:
                return

            outfit_copy = outfit_copy.crop(
                (0, top_y, outfit_copy.width, outfit_copy.height)
            )

            arr = np.array(outfit_copy)
            alpha_channel = arr[:, :, 3]
            result = np.apply_along_axis(
                slice_indexer_raw, axis=1, arr=alpha_channel.astype(np.int32)
            )

            x = sorted(result[:, 0])[1]
            x_neg = sorted(result[:, 1])[-2]

            width, height = outfit_copy.size
            outfit_copy = outfit_copy.crop((x, 0, x_neg, height))

        if args.render_body:
            out_name = os.path.join(
                args.output_dir,
                character_name,
                pose_name,
                "{}.png".format(os.path.splitext(os.path.basename(outfit))[0]),
            )
        else:
            out_name = os.path.join(
                args.output_dir, character_name, pose_name, "{}.png".format(expr_name)
            )
        os.makedirs(os.path.dirname(out_name), exist_ok=True)
        outfit_copy.save(out_name)


def main(args):
    data = []
    characters = [
        item for item in glob(os.path.join(args.input_dir, "*")) if os.path.isdir(item)
    ]
    for character in characters:
        character_name = os.path.split(character)[1]
        try:
            with open(os.path.join(character, "character.yml"), "r") as f:
                char_data = yaml.safe_load(f)
        except:
            with open(os.path.join(character, "character.json"), "r") as f:
                char_data = json.load(f)
        mutations = char_data.get("mutations", None)
        poses = [
            item for item in glob(os.path.join(character, "*")) if os.path.isdir(item)
        ]
        for pose in poses:
            pose_name = os.path.split(pose)[1]
            faces = glob(os.path.join(pose, "faces", "face", "*.png"))
            faces += glob(os.path.join(pose, "faces", "face", "*.webp"))
            faces = sorted(faces, key=natural_sort_key)
            outfits = glob(os.path.join(pose, "outfits", "*.png"))
            outfits += glob(os.path.join(pose, "outfits", "*.webp"))
            accessorized_outfit = False
            outfits += glob(os.path.join(pose, "outfits", "*", "*.png"))
            outfits += glob(os.path.join(pose, "outfits", "*", "*.webp"))
            accessorized_outfit = True
            outfit = None
            og_faces = copy.deepcopy(faces)

            outfit_data = {
                os.path.splitext(os.path.basename(outfit))[0]: outfit
                for outfit in outfits
            }

            if not args.render_body:
                if "uniform" in outfit_data:
                    outfits = [outfit_data["uniform"]]
                elif "casual" in outfit_data:
                    outfits = [outfit_data["casual"]]
                elif "nude" in outfit_data:
                    outfits = [outfit_data["nude"]]
                elif character_name == "elizabeth":
                    print(outfit_data)
                    if "maid" in outfit_data:
                        outfits = [outfit_data["maid"]]
                    else:
                        try:
                            outfits = [outfit_data["formal"]]
                        except KeyError:
                            print("Elizabeth's outfits has changed, be check on it")
                            outfits = [outfits[0]]
                else:
                    outfits = [outfits[0]]

            for outfit in outfits:
                faces = copy.deepcopy(og_faces)
                outfit_name = os.path.splitext(os.path.basename(outfit))[0]
                if mutations:
                    selected_mutation = None
                    for name, mutation_outfits in mutations.items():
                        if outfit_name in mutation_outfits:
                            selected_mutation = name
                            break
                    if selected_mutation:
                        mutation_faces = glob(
                            os.path.join(
                                pose, "faces", "mutations", name, "face", "*.png"
                            )
                        )
                        mutation_faces += glob(
                            os.path.join(
                                pose, "faces", "mutations", name, "face", "*.webp"
                            )
                        )
                        if mutation_faces:
                            faces = sorted(mutation_faces, key=natural_sort_key)
                data.append(
                    (
                        character_name,
                        pose_name,
                        faces,
                        outfit,
                        accessorized_outfit,
                        args,
                    )
                )

    if os.name == "nt" or args.num_threads == 1:
        for item in tqdm(data, total=len(data), unit="poses", disable=args.quiet):
            extract_faces(item)
    else:
        for _ in tqdm(
            worker_pool.imap_unordered(extract_faces, data),
            total=len(data),
            unit="poses",
            disable=args.quiet
        ):
            pass
        