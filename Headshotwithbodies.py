import argparse
from os.path import exists
import os
from glob import glob
from multiprocessing import  cpu_count
from shutil import rmtree

from tqdm import tqdm

from headshot_custom import check, natural_sort_key, render_collage_with_body, main


def get_data(args):
    data = []
    characters = [
        item for item in glob(os.path.join(args.face_dir, "*")) if os.path.isdir(item)
    ]
    for character in characters:
        character_name = os.path.split(character)[1]
        poses = [
            item for item in glob(os.path.join(character, "*")) if os.path.isdir(item)
        ]
        for pose in poses:
            pose_name = os.path.split(pose)[1]
            faces = glob(os.path.join(pose, "*.png"))
            faces += glob(os.path.join(pose, "*.webp"))
            faces = sorted(faces, key=natural_sort_key)
            bodies = []
            if args.bodies_dir:
                bodies = glob(os.path.join(args.bodies_dir, character_name, pose_name, "*.png"))
                bodies += glob(os.path.join(args.bodies_dir, character_name, pose_name, "*.webp"))
                bodies = sorted(bodies, key=natural_sort_key)
                data.append((character_name, pose_name, faces, bodies, args))
            else:
                data.append((character_name, pose_name, faces, args))
    return data

def main_wrap(args):
    
    check(args)
    
    if args.render_faces:
        bodyarg = args.render_body #Make body false to render faces
        out_dir = args.output_dir #Change the output dir to the face dir
        args.output_dir = args.face_dir
        args.render_body = False
        print("Creating faces at "+ os.path.abspath(args.face_dir))
        main(args)
        args.render_body = bodyarg
        args.output_dir = out_dir
        
    if args.render_body:
        out_dir = args.output_dir #Change the output dir to the face dir
        args.output_dir = args.bodies_dir
        print("Creating bodies at "+ os.path.abspath(args.bodies_dir))
        main(args)
        args.output_dir = out_dir
    data = get_data(args)
    
    print("Combining faces and bodies at " + os.path.abspath(args.output_dir))
    for item in tqdm(data, total=len(data), unit="poses", disable=args.quiet) if not args.sheets else []:
        # if item[0] == "sadie":
        #     render_collage_with_body(item)
        render_collage_with_body(item)

    if (not args.skip and exists(args.face_dir)) and not args.sheets:
        rmtree(args.face_dir)
    if (not args.skip and exists(args.bodies_dir)) and not args.sheets:
        rmtree(args.bodies_dir)


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_dir",
        type=str,
        help="(required) The directory containing the character files",
    )
    parser.add_argument(
        "face_dir",
        type=str,
        help="(required) The directory containing the face files",
    )
    parser.add_argument(
        "bodies_dir",
        type=str,
        help="(required) The directory containing the bodies",
    )
    parser.add_argument(
        "output_dir",
        type=str,
        help="(required) The directory to output data to",
    ),
    parser.add_argument(
        "-a",
        "--accessories",
        action="store_true",
        dest="accessory_switch",
        help="If given, enables all accessories",
    )
    parser.add_argument(
        "-b",
        "--createbodies",
        action="store_true",
        dest="render_body",
        help="If given, renders full body shots to given body directory",
    )
    parser.add_argument(
        "-f",
        "--createfaces",
        action="store_true",
        dest="render_faces",
        help="If given, renders faces to given face directory",
    )
    parser.add_argument(
        "-y",
        "--yes",
        action="store_true",
        dest="yes",
        help="If given, answers all script questions with 'yes'",
    )
    parser.add_argument(
        "-t",
        "--threads",
        type=int,
        dest="num_threads",
        default=cpu_count(),
        metavar="N",
        help="The amount of threads to use (default: {}). No windows implementation.".format(cpu_count()),
    )
    parser.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        dest="quiet",
        help="If given, disabled progress output",
    )
    group1 = parser.add_argument_group("Sheet options")
    group = group1.add_mutually_exclusive_group()
    group.add_argument(
        "-s",
        "--skipdelete",
        action="store_true",
        dest="skip",
        help="If given, do not delete face and bodies",
    )
    group.add_argument(
        "-e",
        "--exitbeforeexpressions",
        action="store_true",
        dest="sheets",
        help="If given, will not generate expression sheets",
    )
    group1 = parser.add_argument_group("Body output options")
    group = group1.add_mutually_exclusive_group()
    group.add_argument(
        "-uh",
        "--underheight",
        action="store_true",
        dest="height",
        help="Do not let bodies stack be bigger than face_height*rows (Excluding 1)",
    )
    group.add_argument(
        "-oh",
        "--overheight",
        action="store_true",
        dest="height2",
        help="Let bodies be bigger than faces but only by 1. I face is bigger than 1 body, add 1 until it's not",
    )
    args = parser.parse_args()
    main_wrap(args)
if __name__ == "__main__":
    parse()